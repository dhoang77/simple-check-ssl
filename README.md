simple-check-ssl.sh
===================


## What is it
A simple Bash script using `openssl`, `cut` and `date` utilities to get the certificate of the given host and returns the validity dates and the number of remaining days before expiration.

If the certificat is expired, then instead of the remaining number of days, it will display `Expired`.

This script is very simple / stupid and is not meant to be robust and secure.

So *USE IT AT YOUR OWN RISK*, you are warned.


## Return code

Here are the return codes of the scripts.

Return code | Description
------------|-------------
0           | the certificate is still valid.
2           | the certificate has expired.
something else | there an error.


## Example of use

```Shell
$ ./simple-check-ssl.sh www.google.com
www.google.com | 2017-09-26 10:59:00+0000 | 2017-12-19 10:59:00+0000 | 73 days
$ echo $?
0
$ 
```


## Licence

This tiny project is licensed under MIT Licence.

See [Comparison of free and open-source software licenses - Wikipedia](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses) for more information.
