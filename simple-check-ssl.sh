#!/bin/bash
#
# 2017-10-06: D.S.H.
#
# Under MIT License
#
# Inspired from:
# - https://www.admon.org/faqs/how-to-check-ssl-certificate-expiration-date-from-command-line/
# - https://github.com/Matty9191/ssl-cert-check / http://prefetch.net/articles/checkcertificate.html
#
WEB_SITE=$1

get_serv_cert_validity_dates() {
WEB_HOST=$1
echo | \
  openssl s_client -connect ${WEB_HOST}:443 -servername ${WEB_HOST} 2> /dev/null | \
  openssl x509 -noout -dates
}

iso8601_ts() {
  date --utc --date="$1" '+%Y-%m-%d %H:%M:%S%z'
}

RC=0

TMP_CERT_VAL_DTS=$(get_serv_cert_validity_dates ${WEB_SITE})

VALID_BEG=$(iso8601_ts "$( echo "${TMP_CERT_VAL_DTS}" | grep '^notBefore' | cut -d'=' -f 2 )")
VALID_END=$(iso8601_ts "$( echo "${TMP_CERT_VAL_DTS}" | grep '^notAfter' | cut -d'=' -f 2 )")
REMAINING_DAYS=$(( ($(date --utc --date="${VALID_END}" '+%s') - $(date --utc '+%s'))/86400 ))
if [ ${REMAINING_DAYS} -gt 1 ]; then
  REMAINING_DAYS="${REMAINING_DAYS} days"
elif [ ${REMAINING_DAYS} -eq 1 ]; then
  REMAINING_DAYS="${REMAINING_DAYS} day"
else
  REMAINING_DAYS="Expired"
  RC=2
fi

echo "${WEB_SITE} | ${VALID_BEG} | ${VALID_END} | ${REMAINING_DAYS}"
exit ${RC}
